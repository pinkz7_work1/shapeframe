/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author ripgg
 */
public class CircleFrame {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Circle");
        frame.setSize(500,300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblRadius = new JLabel("Radius: ",JLabel.TRAILING); 
        lblRadius.setSize(50,20);
        lblRadius.setLocation(5,5);
        lblRadius.setBackground(Color.CYAN);
        lblRadius.setOpaque(true);
        frame.add(lblRadius);
        
        final JTextField txtRadius = new JTextField();
        txtRadius.setSize(50,20);
        txtRadius.setLocation(60,5);
        frame.add(txtRadius);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100,20);
        btnCalculate.setLocation(120,5);
        frame.add(btnCalculate);
        
        final JLabel lblResult = new JLabel("Circle Radius =   | Area =   | Perimeter =              ");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500,50);
        lblResult.setLocation(0,50);
        lblResult.setBackground(Color.LIGHT_GRAY);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    String strRadius = txtRadius.getText();
                    double radius = Double.parseDouble(strRadius);
                    Circle circle = new Circle(radius);
                    lblResult.setText("Circle Radius = "+String.format("%.2f",circle.getRadius())
                            +" | Area = " +String.format("%.2f", circle.calArea())
                            +" | Perimeter = " +String.format("%.2f", circle.calPerimeter()));
            }catch(Exception ex){
                    JOptionPane.showMessageDialog(frame, "Error: Plase Input Number ", "Error Na", JOptionPane.ERROR_MESSAGE);
                    txtRadius.setText("");
                    txtRadius.requestFocus();
            }
        }
        });
        
        frame.setVisible(true);
    }
}
