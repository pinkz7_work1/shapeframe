/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author ripgg
 */
public class RectangleFrame {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Rectangle");
        frame.setSize(500,300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblWidth = new JLabel("Width: ",JLabel.TRAILING); 
        lblWidth.setSize(50,20);
        lblWidth.setLocation(10,5);
        lblWidth.setBackground(Color.CYAN);
        lblWidth.setOpaque(true);
        frame.add(lblWidth);
        
        final JTextField txtWidth = new JTextField();
        txtWidth.setSize(50,20);
        txtWidth.setLocation(60,5);
        frame.add(txtWidth);
        
        JLabel lblLenght = new JLabel("Lenght: ",JLabel.TRAILING); 
        lblLenght .setSize(50,20);
        lblLenght .setLocation(120,5);
        lblLenght .setBackground(Color.CYAN);
        lblLenght .setOpaque(true);
        frame.add(lblLenght);
        
        final JTextField txtLenght = new JTextField();
        txtLenght.setSize(50,20);
        txtLenght.setLocation(170,5);
        frame.add(txtLenght);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100,20);
        btnCalculate.setLocation(240,5);
        frame.add(btnCalculate);
        
        final JLabel lblResult = new JLabel("Rectangle Width =   | Lenght =   | Area =   | Perimeter =                ");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500,50);
        lblResult.setLocation(0,50);
        lblResult.setBackground(Color.LIGHT_GRAY);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    String strWidth = txtWidth.getText();
                    String strLenght = txtLenght.getText();
                    double width = Double.parseDouble(strWidth);
                    double lenght = Double.parseDouble(strLenght);
                    Rectangle rectangle = new Rectangle(width,lenght);
                    lblResult.setText("Rectangle Width = "+String.format("%.2f",rectangle.getWidth())
                            +" | Lenght = "+String.format("%.2f",rectangle.getLenght())
                            +" | Area = " +String.format("%.2f", rectangle.calArea())
                            +" | Perimeter = " +String.format("%.2f", rectangle.calPerimeter()));
            }catch(Exception ex){
                    JOptionPane.showMessageDialog(frame, "Error: Plase Input Number ", "Error Na", JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtLenght.setText("");
                    txtWidth.requestFocus();
            }
        }
        });
        
        frame.setVisible(true);
    }
}