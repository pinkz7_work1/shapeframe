/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author ripgg
 */
public class TriangleFrame {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Triangle");
        frame.setSize(500,300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblBase = new JLabel("Base: ",JLabel.TRAILING); 
        lblBase.setSize(50,20);
        lblBase.setLocation(10,5);
        lblBase.setBackground(Color.CYAN);
        lblBase.setOpaque(true);
        frame.add(lblBase);
        
        final JTextField txtBase = new JTextField();
        txtBase.setSize(50,20);
        txtBase.setLocation(60,5);
        frame.add(txtBase);
        
        JLabel lblHigh = new JLabel("High: ",JLabel.TRAILING); 
        lblHigh.setSize(50,20);
        lblHigh.setLocation(120,5);
        lblHigh.setBackground(Color.CYAN);
        lblHigh.setOpaque(true);
        frame.add(lblHigh);
        
        final JTextField txtHigh = new JTextField();
        txtHigh.setSize(50,20);
        txtHigh.setLocation(170,5);
        frame.add(txtHigh);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100,20);
        btnCalculate.setLocation(240,5);
        frame.add(btnCalculate);
        
        final JLabel lblResult = new JLabel("Triangle Base =   | High =   | Area =   | Perimeter =              ");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500,50);
        lblResult.setLocation(0,50);
        lblResult.setBackground(Color.LIGHT_GRAY);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    String strBase = txtBase.getText();
                    String strHigh = txtHigh.getText();
                    double base = Double.parseDouble(strBase);
                    double high = Double.parseDouble(strHigh);
                    Triangle triangle = new Triangle(base,high);
                    lblResult.setText("Rectangle Base = "+String.format("%.2f",triangle.getBase())
                            +" | High = "+String.format("%.2f",triangle.getHigh())
                            +" | Area = " +String.format("%.2f", triangle.calArea())
                            +" | Perimeter = " +String.format("%.2f", triangle.calPerimeter()));
            }catch(Exception ex){
                    JOptionPane.showMessageDialog(frame, "Error: Plase Input Number ", "Error Na", JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtHigh.setText("");
                    txtBase.requestFocus();
            }
        }
        });
        
        frame.setVisible(true);
    }
}
