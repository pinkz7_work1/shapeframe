/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.shapeframe;

/**
 *
 * @author ripgg
 */
public class Rectangle extends Shape {

    private double width;
    private double lenght;

    public Rectangle(double width, double lenght) {
        super("Rectangle");
        this.width = width;
        this.lenght = lenght;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLenght() {
        return lenght;
    }

    public void setLenght(double lenght) {
        this.lenght = lenght;
    }

    @Override
    public double calArea() {
        return width * lenght;
    }

    @Override
    public double calPerimeter() {
        return (width*2)+(lenght*2);
    }

}
